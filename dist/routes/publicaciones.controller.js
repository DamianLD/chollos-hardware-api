"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deletePublicacion = exports.updatePublicacion = exports.createPublicacion = exports.getPublicaciones = exports.getPublicacion = void 0;
const Publicacion_1 = __importDefault(require("./Publicacion"));
const getPublicacion = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const publicacionEncontrada = yield Publicacion_1.default.findById(req.params.id);
    if (!publicacionEncontrada) {
        return res.status(204).json();
    }
    return res.json(publicacionEncontrada);
});
exports.getPublicacion = getPublicacion;
const getPublicaciones = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const publicaciones = yield Publicacion_1.default.find();
        return res.json(publicaciones);
    }
    catch (error) {
        res.json(error);
    }
});
exports.getPublicaciones = getPublicaciones;
const createPublicacion = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const publicacion = new Publicacion_1.default(req.body);
    const guardarPublicacion = yield publicacion.save();
    res.json(guardarPublicacion);
});
exports.createPublicacion = createPublicacion;
const updatePublicacion = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const publicacionActualizada = yield Publicacion_1.default.findByIdAndUpdate(req.params.id, req.body, { new: true });
    if (!publicacionActualizada) {
        return res.status(204).json();
    }
    return res.json(publicacionActualizada);
});
exports.updatePublicacion = updatePublicacion;
const deletePublicacion = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const publicacionEncontrada = yield Publicacion_1.default.findByIdAndDelete(req.params.id);
    if (!publicacionEncontrada) {
        return res.status(204).json();
    }
    return res.json(publicacionEncontrada);
});
exports.deletePublicacion = deletePublicacion;
