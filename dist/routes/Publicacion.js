"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const publicacionSchema = new mongoose_1.Schema({
    titulo: {
        type: String,
        required: true,
        trim: true,
    },
    descripcion: {
        type: String,
        required: true,
        trim: true,
    },
    precio: {
        type: Number,
        required: true,
    },
    ruta_imagen: {
        type: String,
        trim: true,
    }
}, {
    versionKey: false,
    timestamps: true,
});
exports.default = mongoose_1.model('publicaciones', publicacionSchema);
