import mongoose, { ConnectionOptions } from 'mongoose'
import config from './config'

//const MONGODB_URI = `mongodb://${config.MONGO_HOST}/${config.MONGO_DATABASE}`;
const MONGODB_URI = `mongodb+srv://damianld:proxectofct@cluster0.2rjyr.mongodb.net/${config.MONGO_DATABASE}?retryWrites=true&w=majority`;

(async () => {
  try {
    const mongooseOptions: ConnectionOptions = {
      useUnifiedTopology: true,
      useNewUrlParser: true
    }
    const db = await mongoose.connect(MONGODB_URI, mongooseOptions)
    console.log('Database is connected to:', db.connection.name)
  } catch (error) {
    console.error(error)
  }
})()