import { model, Schema } from "mongoose";


const publicacionSchema = new Schema({
  titulo: {
    type: String,
    required: true,
    trim: true,
  },
  descripcion: {
    type: String,
    required: true,
    trim: true,
  },
  precio: {
    type: Number,
    required: true,
  },
  ruta_imagen: {
    type: String,
    trim: true,
  }
}, {
    versionKey: false,
    timestamps: true,
});

export default model('publicaciones', publicacionSchema);