import { RequestHandler } from "express"
import Publicacion from "./Publicacion"

export const getPublicacion: RequestHandler  = async (req, res) => {
  const publicacionEncontrada = await Publicacion.findById(req.params.id)
  if (!publicacionEncontrada) {
    return res.status(204).json()
  }
  return res.json(publicacionEncontrada);
}

export const getPublicaciones: RequestHandler  = async (req, res) => {
  try {
    const publicaciones = await Publicacion.find()
    return res.json(publicaciones)
  } catch (error) {
    res.json(error)
  }
}

export const createPublicacion: RequestHandler  = async (req, res) => {
  const publicacion = new Publicacion(req.body)
  const guardarPublicacion = await publicacion.save()
  res.json(guardarPublicacion)
}

export const updatePublicacion: RequestHandler  = async (req, res) => {
  const publicacionActualizada = await Publicacion.findByIdAndUpdate(req.params.id, req.body, {new: true})
  if (!publicacionActualizada) {
    return res.status(204).json()
  }
  return res.json(publicacionActualizada)
}

export const deletePublicacion: RequestHandler  = async (req, res) => {
  const publicacionEncontrada = await Publicacion.findByIdAndDelete(req.params.id)
  if (!publicacionEncontrada) {
    return res.status(204).json()
  }
  return res.json(publicacionEncontrada);
}
