import { Router } from "express"
const router = Router()

import * as publicacionCtrl from './publicaciones.controller'

router.get('/publicaciones', publicacionCtrl.getPublicaciones)
router.get('/publicaciones/:id', publicacionCtrl.getPublicacion)
router.post('/publicaciones', publicacionCtrl.createPublicacion)
router.put('/publicaciones/:id', publicacionCtrl.updatePublicacion)
router.delete('/publicaciones/:id', publicacionCtrl.deletePublicacion)

export default router